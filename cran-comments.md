## Test environments
* local OS X install, R 4.4.2
* docker rocker/verse:latest
* win-builder (devel, release and oldrelease)

## R CMD check results

0 errors | 0 warnings | 0 note

* The DESRIPTION contains the name of the algorithm `SVM-Maj` which may
  be mistakenly seen as a mis-spelled word
